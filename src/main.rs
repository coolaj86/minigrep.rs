extern crate grep;

use std::env;
use std::process;

fn main() {
    let args: Vec<String> = env::args().collect();

    let conf = grep::Config::new(&args).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    });
    //let filename = &conf.filename.clone();

    if let Err(e) = grep::run(conf) {
        eprintln!("Big bad bada boom!\n{:?}", e);
        //println!("Big bad bada boom!\n{:?}", e.kind());
        //println!("Big bad bada boom!\n{}", e.description());

        process::exit(1);
    }
}
