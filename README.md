grep.rs
=======

This is my personal exploration of the sample grep project in Chapter 12 of "The Rust Book".

Install Rust
------------

To run this project you first need to install rust:

```bash
curl https://sh.rustup.rs -sSf | sh
```

If you already have rust installed you can update it to the latest version like so:

```bash
restup update
```

Install grep.rs
---------------

```bash
git clone https://git.coolaj86.com/coolaj86/grep.rs
pushd rust.rs/
```

History
-------

```bash
cargo init --bin --name grep
cargo run

git clone --depth=1 https://github.com/rust-lang/rust.vim.git ~/.vim/bundle/rust.vim
git clone --depth=1 https://github.com/majutsushi/tagbar.git ~/.vim/bundle/tagbar
echo 'let g:tagbar_ctags_bin = "/Users/aj/github.com/universal-ctags/ctags/ctags"' >> ~/.vimrc
echo 'let g:rustfmt_autosave = 1' >> ~/.vimrc
```
